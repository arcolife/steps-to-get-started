steps-to-get-started
====================

a collection of steps-to-get-started for developmental work

## Note

- This repo is meant to be a personal reference. Use at your own risk!
- Most of these docs are old. Some packages/steps might have been deprecated by now.
